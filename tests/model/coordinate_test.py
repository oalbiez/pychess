# -*- coding: utf-8 -*-
from chess.model.coordinate import Coordinate


def test_coordinate_should_be_parsable() -> None:
    assert Coordinate.parse("a2") == Coordinate.on("A", 2)
