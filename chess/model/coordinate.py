# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from typing import Literal, cast

Column = Literal["A", "B", "C", "D", "E", "F", "G", "H"]
Row = Literal[1, 2, 3, 4, 5, 6, 7, 8]


def validate_column(value: str) -> Column:
    if value in "ABCDEFGH":
        return cast(Column, value)
    raise ValueError(f"'{value}' is not a valid column")


def validate_row(value: str | int) -> Row:
    if isinstance(value, str):
        if value in "12345678":
            return cast(Row, int(value))
    if isinstance(value, int):
        if value in [1, 2, 3, 4, 5, 6, 7, 8]:
            return cast(Row, int(value))
    raise ValueError(f"'{value}' is not a valid row")


@dataclass(frozen=True)
class Coordinate:
    column: Column
    row: Row

    @staticmethod
    def on(column: Column, row: Row) -> "Coordinate":
        return Coordinate(column, row)

    @staticmethod
    def parse(value: str) -> "Coordinate":
        normalized = value.upper().strip()
        return Coordinate(validate_column(normalized[0]), validate_row(normalized[1]))
